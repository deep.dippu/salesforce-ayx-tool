# Copyright (C) 2020 Alteryx, Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Example input tool."""
from ayx_plugin_sdk.core import (
    FieldType,
    InputConnectionBase,
    Metadata,
    Plugin,
    ProviderBase,
    RecordPacket,
    register_plugin,
)
from ayx_plugin_sdk.core.exceptions import WorkflowRuntimeError


class SalesforceTool(Plugin):
    """Concrete implementation of an AyxPlugin."""

    def __init__(self, provider: ProviderBase) -> None:
        """Construct a plugin."""
        self.provider = provider
        self.tool_config = provider.tool_config
        self.config_value = self.tool_config["Value"]
        self.output_anchor = self.provider.get_output_anchor("Output")

        self.provider.io.info(self.tool_config)

        self.output_metadata = Metadata()

        if float(self.config_value) > 0.5:
            raise WorkflowRuntimeError(
                "Values greater than 0.5 are not allowed.")

        # self.provider.io.info(self.tool_config)

        self.provider.io.info("Plugin initialized.")

    def on_input_connection_opened(self, input_connection: InputConnectionBase) -> None:
        """Initialize the Input Connections of this plugin."""
        raise NotImplementedError("Input tools don't have input connections.")

    def on_record_packet(self, input_connection: InputConnectionBase) -> None:
        """Handle the record packet received through the input connection."""
        raise NotImplementedError("Input tools don't receive packets.")

    def on_complete(self) -> None:
        """Create all records."""
        import pandas as pd
        from salesforce import Salesforce

        client_id = self.tool_config['client-id']
        client_secret = self.tool_config['client-secret']
        username = self.tool_config['username']
        password = self.tool_config['password']
        query = self.tool_config['query']

        sf = Salesforce(client_id, client_secret, username, password)
        result = sf.execute(query)

        for col in result.columns:
            self.output_metadata.add_field(col, FieldType.string, size=200)

        self.output_anchor.open(self.output_metadata)

        packet = RecordPacket.from_dataframe(self.output_metadata, result)

        self.output_anchor.write(packet)
        self.provider.io.info("Completed processing records.")


AyxPlugin = register_plugin(SalesforceTool)
