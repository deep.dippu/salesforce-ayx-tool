import requests
import pandas as pd


_AUTH_URL = 'https://login.salesforce.com/services/oauth2/token'


class Salesforce:
    def __init__(self, client_id: str, client_secret: str, user: str, password: str):
        self.client_id = client_id
        self.client_secret = client_secret
        self.user = user
        self.password = password

        self._authenticate()

        self.headers = {
            'Content-type': 'application/json',
            'Accept-Encoding': 'gzip',
            'Authorization': f'{self.token_type} {self.access_token}'
        }

    def _authenticate(self) -> None:
        params = {
            'grant_type': 'password',
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'username': self.user,
            'password': self.password
        }
        res = requests.post(_AUTH_URL, params=params)

        if res.status_code == 200:
            res = res.json()
            self.access_token = res['access_token']
            self.instance_url = res['instance_url']
            self.token_type = res['token_type']
        else:
            raise Exception  # TODO fix with custom exception

    def execute(self, query: str) -> dict:
        action = '/services/data/v39.0/query/'
        query_url = self.instance_url + action

        params = {'q': query}
        response = requests.get(query_url, headers=self.headers, params=params)
        if response.status_code == 200:
            return self.parse(response.json())
        else:
            raise Exception  # TODO: replace with custom exception

    def parse(self, json: dict) -> pd.DataFrame:
        records = json['records']
        fields_to_ignore = {'attributes'}
        fields = list(set(records[0].keys()) - fields_to_ignore)
        data = dict.fromkeys(fields, [])

        for record in records:
            for field in fields:
                data[field].append(record[field])

        return pd.DataFrame(data)


if __name__ == '__main__':
    client_id = '3MVG9n_HvETGhr3BB5BzPeDmKAXhpGJB8mwhvzV9pDZKaHLh5KsaVQ2ESDTLv.ZNuZ0QYNZbLIz2X1LpFgCu6',
    client_secret = '4429A9F9C57FA6F88AD6572D11856424A31C978EF6967011309DCE0C5B000D6A',
    username = 'krish@221b.com',
    password = 'potterforce1'

    query = '''\
            SELECT \
                Name, Email, Title \
            FROM \
                Contact \
            ORDER BY \
                Name DESC'''

    sf = Salesforce(client_id, client_secret, username, password)
    result = sf.execute(query)

"""
client_id
3MVG9n_HvETGhr3BB5BzPeDmKAXhpGJB8mwhvzV9pDZKaHLh5KsaVQ2ESDTLv.ZNuZ0QYNZbLIz2X1LpFgCu6

client_secret
4429A9F9C57FA6F88AD6572D11856424A31C978EF6967011309DCE0C5B000D6A

username
krish@221b.com

password
potterforce1

query
SELECT 
    Name, Email, Title
FROM 
    Contact
ORDER BY 
    Name DESC
"""

